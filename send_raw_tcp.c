#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/udp.h>
#include <netinet/tcp.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "net_utils.h"

#define P 25

int main(){
  int s = socket (PF_INET, SOCK_RAW, IPPROTO_TCP);//open RAW socket

  char datagram[4096];/*will contain ip hdr, tcp hdr and payload. We'll point a IP hdr struct at                                       │
  its beginning and a tcp hdr struct after to write tcp to write values int*/
  struct ip *iph = (struct ip *) datagram;
  struct tcphdr *tcph = (struct tcphdr *) datagram + sizeof(struct ip);

  struct sockaddr_in sin;/* the sockaddr_in containing the dest. address is used
  in sendto() to determine the datagrams path */

  sin.sin_family = AF_INET;
  sin.sin_port = htons (P);/* you byte-order >1byte header values to network
  byte order (not needed on big endian machines) */
  sin.sin_addr.s_addr = inet_addr("127.0.0.1");

  memset(datagram, 0, 4096); /* zero out the buffer */

  /* we'll now fill in the ip/tcp header values */
  iph->ip_hl = 5; //ip header len in "32bit octet". ex: hl=5 means 20 bytes (5*4)
  iph->ip_v = 4; //ip protocol version (4 for IPv4)
  iph->ip_tos = 0; /* type of service for packet priority (0 is normal). First 3 bits are for routing priority
                    The nest 4 bits are for type or service: delay, throughput, reliability, cost */
  iph->ip_len = sizeof (struct ip) + sizeof (struct tcphdr); /*no payload. This is the total lenght of ip datagram
                including ip header, tcp or udp or icmp header and payload */
  //host to network long. Necessary for little endian machines (intelx86 and ARM, noOP on bigEndian machines)
  iph->ip_id = htonl (54321);/* packet id for handling frag packets. ignore for standalone packet*/
  iph->ip_off = 0; /*fragment offset first 3 bits for frag flag: 1: always 0, 2: do not frag bit,
                    3: more-flag (more frag packets to follow). Next 13 bits is frag offset,
                    number of 8-byte big packets already sent */ 
  iph->ip_ttl = 255; /*time to live*/
  iph->ip_p = 6; /*transport protocol: tcp=6, udp=17, icmp=1*/
  iph->ip_sum = 0;    /* Checksum. Set it to 0 before computing the actual checksum later */
  iph->ip_src.s_addr = inet_addr ("1.2.3.4");/* SYN's can be blindly spoofed */
  iph->ip_dst.s_addr = sin.sin_addr.s_addr;
  //note host to network short: necessary when coontructing packet in little endian machine. Network is "BigEndian"
  tcph->th_sport = htons (1234); /*some random port. Source port*/
  tcph->th_dport = htons (P); //destination port
  tcph->th_seq = random ();/* in a SYN packet, the sequence is a random. The sequence number is used to enumerate the TCP segments. The data
                            in a TCP connection can be contained in any amount of segments (=single tcp
                            datagrams), which will be put in order and acknowledged. For example, if you
                            send 3 segments, each containing 32 bytes of data, the first sequence would be
                            (N+)1, the second one (N+)33 and the third one (N+)65. "N+" because the
                            initial sequence is random.*/
  tcph->th_ack = 0;/* number, and the ack sequence is 0 in the 1st packet. */
  tcph->th_x2 = 0;
  tcph->th_off = 0;   /* first and only tcp segment */
  tcph->th_flags = TH_SYN;  /* initial connection request */
  tcph->th_win = htonl (65535);
  tcph->th_sum = 0; /* if you set a checksum to zero, your kernel IP stack  
                    should fill in the correct checksum during transmission */
  tcph->th_urp = 0;
  
  iph->ip_sum = csum ((unsigned short *) datagram, iph->ip_len >> 1);

  /* finally, it is very advisable to do a IP_HDRINCL call, to make sure
   * that the kernel knows the header is included in the data, and doesn't
   * insert its own header into the packet before our data */

  {       /* lets do it the ugly way.. */
    int one = 1;
    const int *val = &one;
    if (setsockopt (s, IPPROTO_IP, IP_HDRINCL, val, sizeof (one)) < 0)
      printf ("Warning: Cannot set HDRINCL!\n");
  }

  while(1){
    sleep(5);
    if (sendto
        (s /*socket*/,
          datagram, /* buffer conmtaining headers and data */
          iph->ip_len, /* total lenght of datagram */
          0, /* routing flag */
          (struct sockaddr *) &sin, /* socket address */
          sizeof(sin)) < 0)
      printf("error\n");
    else{
      printf(".");
      fflush( stdout );
    }
  }
}
