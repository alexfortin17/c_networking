#include "net_utils.h"
//function to generate header checksum
unsigned short csum(unsigned short *buf, int nwords){
  unsigned long sum;
  for(sum = 0; nwords > 0; nwords--){
    sum += *buf++;
  }
  sum = (sum >> 16) + (sum & 0xffff);
  sum += (sum >> 16);
  return ~sum;//all 0 set to 1 and all 1 set to 0
}


