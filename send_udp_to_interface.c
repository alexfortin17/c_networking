#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/udp.h>
#include <netinet/tcp.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "net_utils.h"

#define PCKT_LEN 8192

int main(int argc, char *argv[]){
  int sd;
  char buffer[PCKT_LEN];

  struct ip *iph = (struct ip *) buffer;// iphdr pointer at beginning of buffer
  struct udphdr *udph = (struct udphdr *) (buffer + sizeof(struct ip));//udphdr pointer after iphdr
  uint16_t *int_data = (uint16_t *) (buffer + sizeof(struct ip) + sizeof(struct udphdr));

  // Source and destination addresses: IP and port
  struct sockaddr_in sin, din;
  int one = 1;
  const int *val = &one;

  memset(buffer, 0, PCKT_LEN);

  if(argc != 5){
    printf("- Invalid parameters!!!\n");
    printf("- Usage %s <source hostname/IP> <source port> <target hostname/IP> <target port>\n", argv[0]);
    exit(-1);
  }
  sd = socket(PF_INET, SOCK_RAW, IPPROTO_UDP);
  if(sd < 0){
    perror("socket() error");
    exit(-1);
  }
  else{
    printf("socket() - Using SOCK_RAW socket and UDP protocol is OK.\n");
  }

  // The source is redundant, may be used later if needed
  // The address family
  sin.sin_family = AF_INET;
  din.sin_family = AF_INET;

  // Port numbers
  sin.sin_port = htons(atoi(argv[2]));//atoi is ascii to integer
  din.sin_port = htons(atoi(argv[4]));
  // IP addresses
  sin.sin_addr.s_addr = inet_addr(argv[1]);
  din.sin_addr.s_addr = inet_addr(argv[3]);

  // Fabricate the IP header or we can use the
  // standard header structures but assign our own values.
  iph->ip_hl = 5;
  iph->ip_v = 4;
  iph->ip_tos = 16; // Low delay
  iph->ip_len = sizeof(struct ip) + sizeof(struct udphdr) + sizeof(uint16_t);
  iph->ip_id = htons(54321);

  iph->ip_ttl = 64; // hops

  iph->ip_p = 17; // UDP

  // Source IP address, can use spoofed address here!!!
  iph->ip_src.s_addr = inet_addr(argv[1]);
  // The destination IP address
  iph->ip_dst.s_addr = inet_addr(argv[3]);
  // Fabricate the UDP header. Source port number, redundant
  udph->uh_sport = htons(atoi(argv[2]));
  // Destination port number
  udph->uh_dport = htons(atoi(argv[4]));
  udph->uh_ulen = htons(sizeof(struct udphdr));

  //Simply writing 155 in the payload
  *int_data = htons(155);

  // Calculate the checksum for integrity
  iph->ip_sum = csum((unsigned short *)buffer, sizeof(struct ip) + sizeof(struct udphdr) + sizeof(uint16_t));
  // Inform the kernel do not fill up the packet structure. we will build our own...
  if(setsockopt(sd, IPPROTO_IP, IP_HDRINCL, val, sizeof(one)) < 0){
    perror("setsockopt() error");
    exit(-1);
  }
  else{
    printf("setsockopt() is OK.\n");
  }

  printf("Trying...\n");
  printf("Using raw socket and UDP protocol\n");
  printf("Using Source IP: %s port: %u, Target IP: %s port: %u.\n", argv[1], atoi(argv[2]), argv[3], atoi(argv[4]));

  int count;
  for(count = 1; count<=20; count++){
    if(sendto(sd, buffer, iph->ip_len, 0, (struct sockaddr *)&sin, sizeof(sin)) < 0){
      perror("sendto() error");
      exit(-1);
    }
    else{
      printf("Count #%u - sendto() is OK.\n", count);
      sleep(5);
    }
  }
  close(sd);
  return 0;
}
